import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-router-demo',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
