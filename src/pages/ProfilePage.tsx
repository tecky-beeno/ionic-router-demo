import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useProfile, useToken } from '../hooks/use-token'
import { routes } from '../Routes'

const ProfilePage: React.FC = () => {
  let title = 'Profile'
  const { setProfile } = useProfile()
  function save() {
    setProfile({ height: 1, weight: 1 })
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.settings}></IonBackButton>
          </IonButtons>
          <IonTitle>{title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{title}</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          <IonItem>
            <IonLabel>Height (cm)</IonLabel>
            <IonInput type="number" min="1" max="300" step="0.1"></IonInput>
          </IonItem>
          <IonItem>Weight (kg)</IonItem>
          <IonInput type="number" min="1" max="300" step="0.1"></IonInput>
        </IonList>

        <IonButton onClick={save}>Save</IonButton>
      </IonContent>
    </IonPage>
  )
}

export default ProfilePage
