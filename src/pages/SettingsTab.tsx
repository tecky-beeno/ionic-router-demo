import {
  IonButton,
  IonContent,
  IonHeader,
  IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useToken } from '../hooks/use-token'
import { routes } from '../Routes'

const SettingsTab: React.FC = () => {
  let title = 'Settings'
  const { token, setToken } = useToken()
  function logout() {
    setToken('')
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{title}</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          {token ? (
            <IonItem routerLink={routes.profile} routerDirection="forward">
              Profile
            </IonItem>
          ) : (
            <IonItem routerLink={routes.login}>Login</IonItem>
          )}
        </IonList>

        <IonButton hidden={!token} onClick={logout}>
          Logout
        </IonButton>
      </IonContent>
    </IonPage>
  )
}

export default SettingsTab
