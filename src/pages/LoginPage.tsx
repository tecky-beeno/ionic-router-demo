import {
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useToken } from '../hooks/use-token'

const LoginPage: React.FC = () => {
  let title = 'Login'
  const { setToken } = useToken()
  function login() {
    setToken('123')
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{title}</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonButton onClick={login}>Login</IonButton>
      </IonContent>
    </IonPage>
  )
}

export default LoginPage
