import {
  IonTabs,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonIcon,
  IonLabel,
} from '@ionic/react'
import { triangle, ellipse, cog, settings } from 'ionicons/icons'
import { Route, Redirect } from 'react-router'
import { useToken, useProfile } from './hooks/use-token'
import LoginPage from './pages/LoginPage'
import NotFoundPage from './pages/NotFoundPage'
import ProfilePage from './pages/ProfilePage'
import SettingsTab from './pages/SettingsTab'
import Tab1 from './pages/Tab1'
import Tab2 from './pages/Tab2'

export let routes = {
  /* tabs */
  home: '/tab/home',
  messages: '/tab/messages',
  settings: '/tab/settings',

  /* inner pages */
  profile: '/profile',
  login: '/login',
}

function ProtectedRoute(props: {
  exact?: boolean
  path: string
  component: React.FC
}) {
  const { token } = useToken()
  const { profile } = useProfile()
  const component = !token
    ? LoginPage
    : !profile
    ? ProfilePage
    : props.component
  return <Route exact={props.exact} path={props.path} component={component} />
}

export function Routes() {
  return (
    <>
      <Route exact path="/">
        <Redirect to={routes.home} />
      </Route>
      <ProtectedRoute path={routes.profile} component={ProfilePage} />
      <ProtectedRoute path={routes.login} component={LoginPage} />
      <Route path="/tab">
        <IonTabs>
          <IonRouterOutlet>
            <ProtectedRoute path={routes.home} component={Tab1} />
            <ProtectedRoute path={routes.messages} component={Tab2} />
            <Route path={routes.settings} component={SettingsTab} />
          </IonRouterOutlet>
          <IonTabBar slot="bottom">
            <IonTabButton tab="tab1" href={routes.home}>
              <IonIcon icon={triangle} />
              <IonLabel>Tab 1</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab2" href={routes.messages}>
              <IonIcon icon={ellipse} />
              <IonLabel>Tab 2</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab3" href={routes.settings}>
              <IonIcon md={settings} ios={cog} />
              <IonLabel>Settings</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </Route>
      <Route component={NotFoundPage} />
    </>
  )
}
